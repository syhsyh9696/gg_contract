require '../../../config/environment'

def create_contracts_record
  Contract.delete_all

  contract_types = %w"HDY建安项目 HDY咨询项目 HDY设备项目"
  first_party_name = %w"HDY"
  second_party_name = %w"GUIGU GUOWO"
  status = [true, false]

  10.times do |i|
    type_rand_num = rand(0..2)
    sp_rand_num = rand(0..1)
    status_rand_num = rand(0..1)
    time_rand_num = rand(365..900)
    price_rand_num = rand(10000000..300000000)
    sign_num = rand(1000..10000)

    Contract.create(
        :name => "#{contract_types[type_rand_num]}-#{second_party_name[sp_rand_num]}-#{sign_num}",
        :first_party_name => first_party_name[0],
        :second_party_name => second_party_name[sp_rand_num],
        :start_date => Time.now,
        :due_date => Time.now + time_rand_num.days,
        :status => status[status_rand_num],
        :price => price_rand_num,
        :num => sign_num
    )
  end
end

def create_amendments_record
  Amendment.delete_all

  valid_contract_array = Contract.pluck(:id)
  amendment_name_list = %w"HDY建安项目变更索赔 HDY咨询项目变更索赔 HDY设备项目变更索赔"
  sign_num = rand(10000..100000)
  100.times do |i|
    alter_num = rand(100000..500000)
    contract_id = valid_contract_array[rand(0..valid_contract_array.size - 1)]
    Amendment.create(
                 :name => "#{amendment_name_list[rand(0..2)]}-#{sign_num}",
                 :alter => alter_num,
                 :signed_date => Time.now - rand(0..365).days,
                 :contract_id => contract_id,
    )
  end
end

create_contracts_record
create_amendments_record
