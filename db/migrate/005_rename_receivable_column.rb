class RenameReceivableColumn < ActiveRecord::Migration
  def change
    rename_column :output_values, :receivable, :receivables
  end
end