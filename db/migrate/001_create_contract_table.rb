class CreateContractTable < ActiveRecord::Migration
  def change
    create_table :contracts do |t|
      t.string :name
      t.string :first_party_name
      t.string :second_party_name
      t.datetime :start_date
      t.datetime :due_date
      t.decimal :price, precision: 18, scale: 2
      t.boolean :status
      t.references :contractual, polymorphic: true, index: true
    end
  end
end