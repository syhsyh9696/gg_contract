class CreateAmendments < ActiveRecord::Migration
  def change
    create_table :amendments do |t|
      t.string :name
      t.decimal :alter, precision: 18, scale: 2
      t.datetime :signed_date
      t.belongs_to :contract, index: true
      t.timestamps null: true
    end
  end
end
