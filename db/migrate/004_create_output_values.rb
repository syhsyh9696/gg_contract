class CreateOutputValues < ActiveRecord::Migration
  def change
    create_table :output_values do |t|
      t.string :name
      t.decimal :receivable, precision: 18, scale: 2
      t.decimal :receipts, precision: 18, scale: 2
      t.decimal :certified_amount, precision: 18, scale: 2
      t.decimal :invoice_amount, precision: 18, scale: 2
      t.datetime :start_date
      t.timestamps null: true
    end
  end
end
