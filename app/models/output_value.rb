class OutputValue < ActiveRecord::Base
  # Columns' name certificated by Xiaolu Liu
  # name:             产值名称
  # receivable:       按总包合同应收款
  # receipts:         总包合同实际收款
  # certified_amount: 总包结算产值
  # invoice_amount:   开具发票金额
  # start_date:       产值开始日期

  unloadable
  belongs_to :contract
end
