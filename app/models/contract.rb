class Contract < ActiveRecord::Base
  unloadable
  acts_as_attachable

  belongs_to :contractual, polymorphic: true
  has_many :amendments, dependent: :destroy
  has_many :output_values, dependent: :destroy

  def dynamic_price
    self.amendments.pluck(:alter).reduce(:+)
  end
end