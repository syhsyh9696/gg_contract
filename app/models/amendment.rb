class Amendment < ActiveRecord::Base
  unloadable
  acts_as_attachable

  belongs_to :contract
end
